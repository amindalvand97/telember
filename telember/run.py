import cablo
from telember.models import LastNumber
from telember.operations import add_member_telegram


@cablo.magic
def run_magic_schedule(body=None, headers=None):
    status, response = add_member_telegram()
    return status, response


@cablo.magic
def run_magic_queue(body=None, headers=None):
    return 200


@cablo.magic
def run_magic_hook(body=None, headers=None):
    return 200


#
# run_magic_schedule()
