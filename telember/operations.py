from telember.models import Numbers, SuccessNumber, FailedNumber, LastNumber, Admin
from cablo.services.telegram.v1.actions import telegram_import_number_to_contacts, telegram_add_to_chat


def add_member_telegram(sleep_time=2, link='@telembertest'):
    # -------------------# Entery & Exit & Data  #-------------------#

    """ Data entry from google sheet """

    num2 = Numbers.A()
    numbers = ['+' + num.number for num in num2]

    print(1)

    admins = Admin.A()
    admin_list = []
    for item in admins:
        session_string = item.session_string
        api_id = item.api_id
        api_hash = item.api_hash

        data = {
            "session_string": session_string,
            "api_id": api_id,
            "api_hash": api_hash
        }
        admin_list.append(data)

    print(2)
    sessions = admin_list

    """ Final output """
    final_list_of_number_failed = []
    final_list_of_number_success = []
    save_the_last_number = numbers[-1]

    # -------------------# division number  #-------------------------#

    total_items = len(numbers)
    num_lists = int(total_items / len(sessions))
    list_division = []
    index = 0

    for i in range(len(sessions)):
        order = 0
        list_1 = []
        list_division.append(list_1)
        for num in numbers:
            list_1.append(numbers[index])
            index += 1
            order += 1
            if order == num_lists:
                break

    print(3)
    # -----------------------# main code #-----------------------------#

    order = 0
    for session in sessions:
        session_string = session["session_string"]
        api_id = session["api_id"]
        api_hash = session["api_hash"]

        numb = list_division[order]

        body_import_contacts = {
            "session_string": session_string,
            "api_id": api_id,
            "api_hash": api_hash,
            "numbers": numb
        }

        status, response = telegram_import_number_to_contacts(body_import_contacts)

        if status in [401, 400]:
            continue

        print(response)
        success_number_import = response["success_number"]

        print(4)

        body_add_chat = {
            "session_string": session_string,
            "api_id": api_id,
            "api_hash": api_hash,
            "numbers": success_number_import,
            "link_chat": link,
            "time_sleep": sleep_time
        }

        status, response = telegram_add_to_chat(body_add_chat)

        if status in [401, 400]:
            continue

        success_number_add = response["success_number"]

        print(5)

        [final_list_of_number_success.append(item) for item in success_number_add]
        order += 1

    # ----------------------# create failed number list #------------------------#

    for item in numbers:
        if item not in final_list_of_number_success:
            final_list_of_number_failed.append(item)

    # ---------------------# return result #-------------------------#

    # return final_list_of_number_success,final_list_of_number_failed,save_the_last_number

    print(6)

    for suc in final_list_of_number_success:
        res = SuccessNumber(success_number=suc)
        res.key = final_list_of_number_success.index(suc) + 1
        res.save()

    print(7)
    for item in final_list_of_number_failed:
        res = FailedNumber(failed_number=item)
        res.key = final_list_of_number_failed.index(item) + 1
        res.save()

    return 200, {"message": "Done"}
